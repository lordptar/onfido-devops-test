FROM ubuntu:trusty

# install dependencies 
RUN apt-get update && apt-get install -y python python-pip
RUN pip install Flask==0.10.1 \
    itsdangerous==0.24 \
    Jinja2==2.8 \
    MarkupSafe==0.23 \
    redis==2.10.5 \
    Werkzeug==0.11.3 \
    wheel==0.26.0 

# install app
RUN mkdir /app
ADD api.py /app
RUN chmod 755 /app/api.py

WORKDIR /app


EXPOSE 5000
 
CMD "python" "api.py"