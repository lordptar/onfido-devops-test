#!/bin/bash

set -e

#set variables
[[ -n $REDIS_HOST ]] && con_redis="-e REDIS_HOST="$REDIS_HOST

[[ -n $ENV ]] && con_env="-e ENV="$ENV

dockerhost=$(ip addr show docker0 | grep inet\ | awk '{print $2}' | cut -d/ -f1)

docker run -d --rm -p 8001:5000 --add-host=dockerhost:$dockerhost $con_redis $con_env pleochroic/devop-test:version2
docker run -d --rm -p 8002:5000 --add-host=dockerhost:$dockerhost $con_redis $con_env pleochroic/devop-test:version2

